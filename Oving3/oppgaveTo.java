/*
 Oppgave 2
Skriv et program som finner ut om et tall er et primtall. Et primtall er et tall som kun kan deles 
med 1 og med seg selv uten å få rest. Les inn tallet fra brukeren og la programmet repetere 
slik at flere tall kan analyseres
 */
import java.util.InputMismatchException;
import java.util.Scanner; 

class oppgaveTo {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);

        String j = "ja";
        while ( j.equalsIgnoreCase("ja")){

            try{

                    System.out.println("Velg et tall du vil sjekke om er primtall");
                    int numb = scanner.nextInt();
                    boolean erPrim; 

                    if (numb < 2){
                        erPrim = false; 
                    } else {
                        erPrim = true; 
                    }

                    for (int i = 2; i <= numb/i; i++){      //dersom numb ikke er primtal vil numb/i gi kvadratrot
                        if ((numb%i)==0) {
                            erPrim = false; 
                            break;
                        }
                    }
                    if(erPrim){
                        System.out.println("Er primtall!");
                    } else {
                        System.out.println("Er IKKE primtall!");
                    }

                    System.out.println("Vil du prøve et tall til?(ja/nei)");
                    scanner.nextLine();                                 //uten denne funker ikke
                    j = scanner.nextLine();                             //hvis brukeren svarer ja så kjører vi på nytt

            } catch(InputMismatchException e) {
                        System.err.println("Feil: Du må skrive et heltall, prøv på nytt!    ");         //feilmeldingen
                       String numb = scanner.next();             // denne tømmer variabelen slik at den ike spinner rundt        
                   }
                  
            }
            
        System.out.println("Takk for spillet, på gjennsyn");









        scanner.close(); 
    }
}
