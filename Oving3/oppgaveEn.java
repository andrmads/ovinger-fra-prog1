
/*
 Lag et program som skriver ut en del av multiplikasjonstabellen, for eksempel fra 13-15. Da 
skal utskriften se omtrent slik ut (prikkene skal erstattes med regnestykker).
13-gangen:
13 x 1 = 13
13 x 2 = 26
…
13 x 10 = 130
14-gangen:
14 x 1 = 14
14 x 2 = 28
…
14 x 10 = 140
15-gangen: 
15 x 1 = 15
15 x 2 = 30
…
15 x 10 = 150
Brukeren skal velge hvilken del av tabellen som skal skrives ut.
 */
import java.util.Scanner; 
import java.util.InputMismatchException;

class oppgaveEn{
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);

       
        String j = "ja";
        while ( j.equalsIgnoreCase("ja")){

            try{
                System.out.println("Skriv et tall du vil ha gangetabellen til:   ");
                int tall = scanner.nextInt();                       //oppretter er variabel tall med ti-tallet brukeren vil ha gangetabellen til
            
                System.out.printf("%d-gangen:", tall);         //skal skrive ut "tall-gangen: "
                System.out.println(" "); //for å unngå at forrige print kommer på samme linje som neste print, f-streng er wierd


                for (int i=1; i<11; i++){
                    System.out.printf("%d x %d = ",tall, i); //Skal skrive ut "tall x i ="
                    System.out.println(tall*i);                     //Skal skrive ut tall*i dvs. svaret
                    
                }
                

                System.out.println("Vil du ha en til?(ja/nei)");
                scanner.nextLine();                                 //uten denne funker ikke
                j = scanner.nextLine();                             //hvis bruker svarer ja kjører vi på nytt
        
            } catch(InputMismatchException e) {                      // dersom brukeren ikke skrievr tall 
                System.err.println("Feil: Du må skrive et heltall, prøv på nytt!    ");         //feilmeldingen
                String tall = scanner.next();                      // denne tømmer variabelen slik at den ike spinner rundt
        }        
        
    }               //slutt på while løkke

    System.out.println("Takk for spillet, på gjennsyn");
    scanner.close();
    }
}