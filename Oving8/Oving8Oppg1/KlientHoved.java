import java.util.Scanner;
import java.util.ArrayList;

public class KlientHoved {
    public static void main(String [] args){

        Scanner scanner = new Scanner(System.in);
        boolean KjørProgram = true; 

        ArrayList<ArbTaker> ansatte = new ArrayList<>(); // oppretter en liste over alle ansatte
        //arrayList<DATATYPE> navn = new ArrayList<<()
        int ansattTeller = 0;                           // holder oversikt over antal ansatte

        while (KjørProgram){
            System.out.println("");
            System.out.println("Du får nå 4 valg: ");
            System.out.println("1: Opprett ny ansatt-bruker");
            System.out.println("2: Se eksisterende ansatt-brukere");
            System.out.println("3: Endre en bruker");
            System.out.println("4: Avslutt programmet");
            System.out.println("Trykk  1, 2, 3, eller 4: ");
            System.out.println("");


            

            int valg = scanner.nextInt();
            scanner.nextLine();
            
        
            switch(valg){
                case 1: 
                    ansattTeller++;
                    System.out.println("Skriv inn ditt fornavn");
                    String ForNavn = scanner.nextLine();
                    System.out.println("Skriv inn ditt etternavn");
                    String EtterNavn = scanner.nextLine();
                    System.out.println("Skriv inn ditt fødelsår");
                    int FødselÅr = scanner.nextInt();

                    int ArbTakerNr = ansattTeller;
                    System.out.println("Skriv inn din lønn");
                    int MndLønn = scanner.nextInt();
                    System.out.println("Skriv inn året du ble ansatt");
                    int AnsettelsesÅr = scanner.nextInt();
                    System.out.println("Skriv inn din skatteprosent");
                    int SkatteProsent = scanner.nextInt();

                

                    Person person = new Person(ForNavn, EtterNavn, FødselÅr);
                    ArbTaker arbtaker = new ArbTaker(person, ArbTakerNr, AnsettelsesÅr, MndLønn, SkatteProsent);
                    ansatte.add(arbtaker);
                    
                    System.out.println("");
                    System.out.println("Ny ArbTakaker har blitt lagt til!");
                    System.out.println("Arbeidstakernummeret til den nye brukeren er " + ArbTakerNr);
                    System.out.println("");


                    break;
                case 2:
                    if(ansatte.size()==0){
                        System.out.println("Det er ingen ansatte lagret");
                    } else{
                        System.out.println("Det er " + ansattTeller + " ansatte registrert");
                        System.out.println("Skriv ArbTakerNr til brukeren du vil se"); //arbTakerNr sammenfaller med plassering i listen over ansatte
                        int nr = scanner.nextInt() -1; //ansatt nr1 vil ha plass nr 0 i listen
                        scanner.nextLine();

                        if(nr<0){
                            throw new IllegalArgumentException("Kun positive tall");
                        } else if (nr>ansatte.size()){
                            int ant = nr+1;
                            System.out.println("Vi har kun registrert " + ant + " ansatte");
                            System.out.println("Vennligst velg et tall mellom 1 og " + ant);
                        } else {
                        System.out.println("");
                        ArbTaker valgtArbTaker = ansatte.get(nr);
                        System.out.println("Info for valgt Arbeidstaker:");
                        System.out.println("Arbeidstaker-nummer: " + valgtArbTaker.getArbTakerNr());
                        System.out.println("Fornavn: " + valgtArbTaker.getPersonalia().getForNavn());
                        System.out.println("Etternavn: " + valgtArbTaker.getPersonalia().getEtterNavn());
                        System.out.println("Navn på formel form: " + valgtArbTaker.getFormelNavn());
                        System.out.println("Fødelsår: " + valgtArbTaker.getPersonalia().getFødselÅr());
                        System.out.println("Alder: " + valgtArbTaker.getAlder());
                        System.out.println("MånedsLønn: " + valgtArbTaker.getMndLønn());
                        System.out.println("Bruttolønn på et år (lønn før skatt): " + valgtArbTaker.getBruttoLønn());
                        System.out.println("Total skatt pr år: " + valgtArbTaker.getSkattPrÅr());
                        System.out.println("Ansettelsesår: " + valgtArbTaker.getAnsettelsesÅr());
                        System.out.println("Antall år ansatt: " + valgtArbTaker.getÅrAnsatt());
                        System.out.println("SkatteProsent: " + valgtArbTaker.getSkatteProsent());
                        System.out.println("Skatt i kroner pr måned: " + valgtArbTaker.getSkattIKroner());
                        System.out.println("");
                        System.out.println("Vil du sjekke om brukeren har vært lengre ansatt enn et gitt antall år?");
                        String svar = scanner.nextLine();
                        if (svar.equalsIgnoreCase("ja")){
                            System.out.println("Skriv in antall år du vil sjekke om han har vært ansatt");
                            int testÅr = scanner.nextInt();
                            System.out.println("Har han vært ansatt mer enn " + testÅr + " år: " + valgtArbTaker.harVærtAnsattLengreEnn(testÅr));
                        }
                        }
                        
                    }
                    break;
                case 3:  
                
                    System.out.println("La oss endre en bruker");
                    System.out.println("Skriv inn ArbTakerNr til brukeren du vil endre");
                    System.out.println("");
            
                    int arbForEndring = scanner.nextInt();
                    int index = -1; // Variabel for å lagre indeksen til arbeidstakeren i ArrayList
            
                    for (int i = 0; i < ansatte.size(); i++) {              // bruker for løkke til å finne arbtaker i liste
                        if (ansatte.get(i).getArbTakerNr() == arbForEndring) {
                            index = i;
                            break;
                        }
                    }
            
                    if (index == -1) {
                        System.out.println("Ingen arbeidstaker med det angitte ArbTakerNr ble funnet.");
                        System.out.println("");
                    } else {
                        boolean fortsettEndre = true;
                        while (fortsettEndre) {
                            ArbTaker valgtArbTaker = ansatte.get(index);
                            System.out.println("Du har valgt Arbeidstaker-nummer: " + valgtArbTaker.getArbTakerNr());
                            System.out.println("Dette er infoen om denne arbeidstakeren: ");
                            System.out.println("Fornavn: " + valgtArbTaker.getPersonalia().getForNavn());
                            System.out.println("Etternavn: " + valgtArbTaker.getPersonalia().getEtterNavn());
                            System.out.println("Fødselsår: " + valgtArbTaker.getPersonalia().getFødselÅr());
                            System.out.println("MånedsLønn: " + valgtArbTaker.getMndLønn());
                            System.out.println("Ansettelsesår: " + valgtArbTaker.getAnsettelsesÅr());
                            System.out.println("SkatteProsent: " + valgtArbTaker.getSkatteProsent());
                            
                            System.out.println("");
                            System.out.println("Hvilken informasjon vil du endre?");
                            System.out.println("1: MånedsLønn");
                            System.out.println("2: Ansettelsesår");
                            System.out.println("3: SkatteProsent");
                            System.out.println("4: Avslutt endringsmodus");
                            System.out.println("");
                
                            int valgForEndring = scanner.nextInt();
                            scanner.nextLine();
                
                            switch (valgForEndring) {
                                
                                case 1:
                                    System.out.println("Skriv inn ny månedslønn: ");
                                    System.out.println("");
                                    int nyMndLønn = scanner.nextInt();
                                    valgtArbTaker.setMndLønn(nyMndLønn);
                                    break;
                                case 2:
                                    System.out.println("Skriv inn nytt ansettelsesår: ");
                                    System.out.println("");
                                    int nyttAnsettelsesÅr = scanner.nextInt();
                                    valgtArbTaker.setAnsettelsesÅr(nyttAnsettelsesÅr);
                                    break;
                                case 3:
                                    System.out.println("Skriv inn ny skatteprosent: ");
                                    System.out.println("");
                                    int nySkatteProsent = scanner.nextInt();
                                    valgtArbTaker.setSkatteProsent(nySkatteProsent);
                                    break;
                                case 4:
                                    fortsettEndre = false;
                                    break;
                                default:
                                    System.out.println("Ugyldig valg for endring. Velg et tall mellom 1 og 7.");
                                    System.out.println("");
                            
                        }
                    }
                }
                break;
            
                case 4:
                    KjørProgram = false;
                    break; 
                default: 
                    System.out.println("Ugyldig valg, prøv igjen. Kun tall mellom 1 og 3 er gyldig");
                    System.out.println("");
                    break; 
            }


        }
        System.out.println("På utsiden av programmet");




        int ÅrForSammenlikning = 50;
        scanner.close();
    }
}
