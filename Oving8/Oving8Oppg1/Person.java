
public class Person {
    private final String ForNavn;       // ordet final gjør attributtene immutable dvs at de ikke kan endres senere
    private final String EtterNavn;
    private final int FødselÅr;

public Person(String ForNavn, String EtterNavn, int FødselÅr){
    this.ForNavn = ForNavn;
    this.EtterNavn = EtterNavn;
    this.FødselÅr = FødselÅr;
} 



    public String getForNavn(){
        return ForNavn;
    }
    public String getEtterNavn(){
        return EtterNavn;
    }
    public int getFødselÅr(){
        return FødselÅr;
    }
   


    
}
