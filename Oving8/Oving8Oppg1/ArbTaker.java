import java.time.*;
import java.time.format.DateTimeFormatter;

public class ArbTaker {
    private Person Personalia;
    private int ArbTakerNr;
    private int AnsettelsesÅr;
    private int MndLønn;
    private int SkatteProsent;

public ArbTaker(Person Personalia, int ArbTakerNr, int AnsettelsesÅr, int MndLønn, int SkatteProsent){
    this.Personalia = Personalia;
    this.ArbTakerNr = ArbTakerNr;
    this.AnsettelsesÅr = AnsettelsesÅr;
    this.MndLønn = MndLønn;
    this.SkatteProsent = SkatteProsent; 
}





    public Person getPersonalia(){
        return Personalia;
    }
    public int getArbTakerNr(){
        return ArbTakerNr;
    }
    public int getAnsettelsesÅr(){
        return AnsettelsesÅr;
    }
    public int getMndLønn(){
        return MndLønn;
    }
    public int getSkatteProsent(){
        return SkatteProsent;





    }
    public void setMndLønn(int NyLønn){
        this.MndLønn = NyLønn;
    }
    public void setSkatteProsent(int NySkatteProsent){
        this.SkatteProsent = NySkatteProsent;
    }
    public void setAnsettelsesÅr(int NyttAnsettelsesÅr){
        this.AnsettelsesÅr = NyttAnsettelsesÅr;  
    }
    
    






    public double getSkattIKroner(){
        double ProsentTallSkatt = SkatteProsent/100.0;      //skriver 100.0 for å unngå at begge variabler er int
        double KronerISkatt = MndLønn*ProsentTallSkatt;
        return KronerISkatt;
    }
    public int getBruttoLønn(){ //total lønn pr år før skatt
        int BruttoLønn = MndLønn*12;
        return BruttoLønn;
    }
    public String getFormelNavn(){
        String FormelNavn = Personalia.getEtterNavn() + ", " + Personalia.getForNavn();
        return FormelNavn;
    }
    public int getAlder(){
        LocalDate DagensDato = LocalDate.now();
        int DetteÅret = DagensDato.getYear();
        int alder = DetteÅret - Personalia.getFødselÅr();
        return alder;
    }
    public int getÅrAnsatt(){
        LocalDate DagensDato = LocalDate.now();
        int DetteÅret = DagensDato.getYear();
        int ÅrAnsatt = DetteÅret - AnsettelsesÅr;
        return ÅrAnsatt;
    }
    
    public boolean harVærtAnsattLengreEnn(int ÅrForSammenlikning){
        LocalDate DagensDato = LocalDate.now();
        int DetteÅret = DagensDato.getYear();
        return (DetteÅret - AnsettelsesÅr)>ÅrForSammenlikning;  //Sammenlikner antall år ansatt med ÅrForSammenlikning - True/false
    }

    public double getSkattPrÅr(){
        double totalSkattPrÅr = getSkattIKroner()*10 + getSkattIKroner()*0.5;
        return totalSkattPrÅr;
    }

    
    



    
}


