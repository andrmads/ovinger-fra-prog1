
public class Terningspillet100 {
    public static void main(String[] args) {
        Spiller spillerA = new Spiller("Per", 0);
        Spiller spillerB = new Spiller("Ole", 0);

        int runde = 1;

        while (true) {
            spillerA.kastTerningen();
            spillerB.kastTerningen();

            System.out.println("");
            System.out.println("Runde: " + runde + ":");
            System.out.println("Spiller A har " + spillerA.getSumPoeng() + " poeng.");
            System.out.println("Spiller B har " + spillerB.getSumPoeng() + " poeng.");

            if (spillerA.erFerdig()) {
                System.out.println("Spiller A har vunnet ");
                break;
            } else if(spillerB.erFerdig()) {
                System.out.println("Spiller B har vunnet ");
                break;
            } 

            runde++;
        }
    }
}





