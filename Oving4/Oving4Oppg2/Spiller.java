
import java.util.Random;
public class Spiller {

    //objektvariabel
    private String Name;
    private int sumPoeng;

    //konstruktør for å opprette spiller-objekt med navn og sumPoeng
    public Spiller(String Name, int sumPoeng){
        this.Name = Name; 
        this.sumPoeng = sumPoeng; 
    }
    

      
    //tilgangsmetode for navn og sumPoeng
    public String getName(){
        return Name;
    }
    public int getSumPoeng(){
        return sumPoeng;
    }


    // mutasjonsmetoder 
    public int kastTerningen(){
        Random random = new Random();
        int terningkast = random.nextInt(6)+1;      //genererer tilfeldig tall mellom 1 og 6
        if (terningkast ==1 ){
            sumPoeng=0;
        } else {
            sumPoeng+=terningkast;
        }
        return sumPoeng;
     }
       


     public boolean erFerdig(){             // sjekker om spiller har nådd/er over 100 poeng 
        return sumPoeng>=100;
    }
    
}





