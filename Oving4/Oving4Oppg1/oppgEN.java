import java.util.Scanner;

public class oppgEN {
    public static void main(String args[]){

        Scanner scanner = new Scanner(System.in);

        valuta Dollar = new valuta("Dollar", 10.70);
        valuta Euro = new valuta("Euro", 11.45);
        valuta Pound = new valuta("Pound", 13.34);

        System.out.println("Velkommen! ");
        
        

        String j = "ja";
        while (j.equalsIgnoreCase("ja")){
            System.out.println("Venligst velg hvilken valuta du vil konvertere");
            System.out.println("Tast 1 for å konvertere NOK til USD");
            System.out.println("Tast 2 for å konvertere USD til NOK");
            System.out.println("Tast 3 for å konvertere NOK til EUR");
            System.out.println("Tast 4 for å konvertere EUR til NOK");
            System.out.println("Tast 5 for å konvertere NOK til GBP");
            System.out.println("Tast 6 for å konvertere GBP til NOK");
            int brukerensValg = scanner.nextInt();

            

            switch(brukerensValg){
                case 1:
                //fra NOK til DOllar
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraNOK1 = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilDollar = Dollar.NOKtoValuta(belopFraNOK1); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilDollar = String.format("%.2f", belopTilDollar); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Dollar.getName() + " er: " + Dollar.getKurs() + " kr");
                System.out.println(belopFraNOK1 + " kroner blir til : " + fBelopTilDollar + " dollar");
                break;
                case 2:
                //Fra Dollar til NOK 
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraDollar = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilNOK1 = Dollar.ValutaToNOK(belopFraDollar); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilNOK1 = String.format("%.2f", belopTilNOK1); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Dollar.getName() + " er: " + Dollar.getKurs() + " kr");
                System.out.println(belopFraDollar + " dollar blir til :  " + fBelopTilNOK1 + " kr");
                break; 

                case 3:
                //Fra NOK til Euro
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraNOK3 = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilEuro = Euro.NOKtoValuta(belopFraNOK3); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilEuro = String.format("%.2f", belopTilEuro); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Euro.getName() + " er: " + Euro.getKurs() + " kr");
                System.out.println(belopFraNOK3 + " kroner blir til : " + fBelopTilEuro + " euro");
                break; 
                case 4:
                //Fra Euro til NOK 
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraEuro = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilNOK4 = Euro.ValutaToNOK(belopFraEuro); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilNOK4 = String.format("%.2f", belopTilNOK4); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Euro.getName() + " er: " + Euro.getKurs() + " kr");
                System.out.println(belopFraEuro + " Euro blir til :  " + fBelopTilNOK4 + " kr");
                break;
                
                case 5:
                //fra NOK til Pound
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraNOK5 = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilPound = Pound.NOKtoValuta(belopFraNOK5); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilPound = String.format("%.2f", belopTilPound); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Pound.getName() + " er: " + Pound.getKurs() + " kr");
                System.out.println(belopFraNOK5 + " kroner blir til : " + fBelopTilPound + " Pound");
                break;
                case 6:
                //fra Pound til NOK 
                System.out.println("Hvor mye vil du konvertere? "); 
                int belopFraPound = scanner.nextInt(); //mengden brukeren ønsker å konvertere 
                double belopTilNOK6 = Pound.ValutaToNOK(belopFraPound); //bruker metoden for å konvertere fra valuta-klassen
                String fBelopTilNOK6 = String.format("%.2f", belopTilNOK6); //formaterer for 2 desimaler
                System.out.println("Kursen på en " + Pound.getName() + " er: " + Pound.getKurs() + " kr");
                System.out.println(belopFraPound + " pound blir til :  " + fBelopTilNOK6 + " kroner");
                break;


            }
            j = scanner.nextLine(); // må tømme scanneren for at scanneren vil fange inputen i linje 90

            System.out.println("Vil du gjøre det igjen?");
            j = scanner.nextLine();
        }

        System.out.println("På gjensyn!");
        scanner.close();
    }
}
