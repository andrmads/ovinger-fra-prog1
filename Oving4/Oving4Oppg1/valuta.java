public class valuta {


    
    private String Name; 
    private double Kurs; 

    //konstruktør for å opprette valutta objekt med navn og kurs
    public valuta(String Name, double Kurs){
        this.Name = Name; 
        this.Kurs = Kurs;
    }

    //tilgangsmetode for først navn så kurs
    public String getName(){
        return Name; 
    }
    public double getKurs(){
        return Kurs; 
    }
    //mutasjonsmetode - metode for å konvertere valutaene

    public double NOKtoValuta(int belopToNOK){
        return belopToNOK / Kurs;
    }
    public double ValutaToNOK(int belopFraValuta){
        return belopFraValuta * Kurs; 
    }
    


    
}