public class Property{

        private int municipalityNumber;     //kommunenummer
        private String municipalityName;    //kommunenavn
        private int lotNumber;              //gårdsnummer aka gnr
        private int sectionNumber;          // bruksnummer aka bnr
        private String nameLot;             // bruksnavn
        private int area;                //areal
        private String nameOwner;           //navn på eier

        //constructor with nameLot
        public Property(int municipalityNumber, String municipalityName, 
            int lotNumber, int sectionNumber, String nameLot, 
            int area, String nameOwner){

                this.municipalityNumber = municipalityNumber;
                this.municipalityName = municipalityName;
                this.lotNumber = lotNumber;
                this.sectionNumber = sectionNumber;
                this.nameLot = nameLot;
                this.area = area;
                this.nameOwner = nameOwner;
                }
        //constructor without nameLot
        public Property(int municipalityNumber, String municipalityName, 
            int lotNumber, int sectionNumber, 
            int area, String nameOwner){

                this.municipalityNumber = municipalityNumber;
                this.municipalityName = municipalityName;
                this.lotNumber = lotNumber;
                this.sectionNumber = sectionNumber;
                this.nameLot = "";
                this.area = area;
                this.nameOwner = nameOwner;
            }



        //access method for the municipality number
        public int getMunicipalityNumber(){
            return municipalityNumber;
        }
        //access method for the municipality name
        public String getMunicipalityName(){
            return municipalityName;
        }
        //access method for the lot number
        public int getLotNumber(){
            return lotNumber;
        }
        //access method for the section number
        public int getSectionNumber(){
            return sectionNumber;
        }
        //access method for the lot name
        public String getNameLot(){
            return nameLot;
        }
        //access method for the area
        public int getArea(){
            return area;
        }
        //access method for the name of the owner
        public String getNameOwner(){
            return nameOwner;
        }

        // set-method for name of owner because the property could be sold and bought
        public void setOwner(String newOwner){
            this.nameOwner = newOwner;
        }

        //to-string method which prints out all info about any one particular property
        @Override
        public String toString(){
            return 
                "MUNICIPALITY: " + getMunicipalityName()
            +", MUNICUPALITY-NUMBER: " + getMunicipalityNumber()
            +", LOT-NUMBER: " + getLotNumber() 
            +", SECTION-NUMBER: " + getSectionNumber()
            +", NAME OF LOT: " + getNameLot()
            +", NAME OF OWNER: " + getNameOwner()
            +", AREA: " + getArea();
        }





}