import java.util.ArrayList;



public class PropertyRegister{
    private ArrayList <Property> properties; // makes an ArrayList of "Property"s which I name properties

//constructor 
public PropertyRegister(){
    properties = new ArrayList<Property>(); 
}



// method for finding a property based on municipalityNumber, sectionNumber and lot-number
public ArrayList<Property> findProperties(int mNumber, int sNumber, int lNumber){
    ArrayList <Property> candidates = new ArrayList<>();             // I create a new list, which for now is empty, but which will store relevant properties
    for (Property property : properties){                       // i iterate through all properties in "properties" which is where all properties are registered
        if (property.getMunicipalityNumber() == (mNumber)       // i create three conditions, and all must be fullfulled 
            && property.getLotNumber() == lNumber
            && property.getSectionNumber() == sNumber){
                candidates.add(property);                       //property which fullfills criterions are added to the list candidates
          
    }
}
return candidates;   
}


//method for removing a property from the register of properties
public void removeProperty(int mNumber, int sNumber, int lNumber){
    boolean x = true;                                           // boolean which is used to filter whether to print "property NOT found"
    for (Property property : properties){                       // iterates through every property in the register of all properties
        if (property.getMunicipalityNumber() == (mNumber)       // i create three conditions, and all must be fullfulled 
            && property.getLotNumber() == lNumber
            && property.getSectionNumber() == sNumber){
                    properties.remove(property);                    // if there is a property in the list which fullfills all conditions, this property is removed
                    System.out.println("The chosen property has successfully been removed!"); 
                    x = false;                                      //boolean is made false to ensure NOT to print that "the property is not found"
                    break;                               
            }
        }
    if(x){                                                      //if x is true the user is alerted that the property is not in the register of properties
        System.out.println("The property was not found in the register"); 
    }      
    }


// method which checks whether a property with certain conditions is registered --- this is helpful for case 3 in the client program
public boolean checkRegisterContains(int mNumber, int sNumber, int lNumber){    //boolean method which takes in three numbers as parameters
    for (Property property : properties){                       // iterates through every property in the register of all properties
        if (property.getMunicipalityNumber() == (mNumber)       // i create three conditions, and all must be fullfulled 
            && property.getLotNumber() == lNumber
            && property.getSectionNumber() == sNumber){
                return true;                                    // if there is a property in the list which fullfills all conditions true is returned
            } 
        }
        return false;                                           // if there is not such a property, false is returned
}



//method for finding average area of all registered properties
public double findAverageArea(){
    double totalArea = 0;               //initialise total area as 0
    int totalNumberProperties = 0;      //initialise total amount of properties as 0 

    for (Property property : properties){  // iterates through all registered properties
        totalNumberProperties += 1;         // for each property the variabel counting the total number of properties increases by one
        totalArea += property.getArea();    // for each propertythe variabel counting the total area increases by the are of that particualr property
    }
    double averageArea = totalArea/totalNumberProperties;   // calculates the average area of all the properties
    return averageArea;                                     // returns the average are of all the properties
}




// method for adding a new property to the register of properties
public void addProperty(Property newProperty){   //takes a new property as parameter                       
        properties.add(newProperty);           //adds this new instance of property to the list of already registered properties
}





// method for printing all properties in the register
@Override
public String toString(){       //uses a toString method
    StringBuilder sb = new StringBuilder();     //creates an empty Strinbuilder as a placeholder for info about the properties
    sb.append("Print of all registered properties: \n"); //gives the Strinbuilder a "headline and then uses "\n" to make a newline
    for (Property property : properties){                    //iterates thorugh each property in the list of registered properties
        sb.append(property.toString()).append("\n");      //each property (in a toString method) is added to the stringbuilder
    }
    return sb.toString();                                   //returns the string of all properties, which all are formated as strings with relevant informasjon 
}

}//edge of class