import java.util.Scanner;


public class Client 
{
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        PropertyRegister propertyReg = new PropertyRegister();

        Property property1 = new Property(1445,"Gloppen",77,631,1017,"Jens Olsen");
        Property property2 = new Property(1445,"Gloppen",77,131,"Syningom", 650,"Nicolay Madsen");
        Property property3 = new Property(1445, "Gloppen",75,19,"Fugletun",650,"Evilyn Jenson");
        Property property4 = new Property(1445,"Gloppen", 74,188,1457, "Karl Ove Braaten");
        Property property5 = new Property(1445,"Gloppen",69,47,1339,"Elsa IndreGaard");

        propertyReg.addProperty(property1);
        propertyReg.addProperty(property2);
        propertyReg.addProperty(property3);
        propertyReg.addProperty(property4);
        propertyReg.addProperty(property5);
        

        while(true){
            System.out.println("");
            System.out.println("Please chose an operation by dialing the corresponding number: ");
            System.out.println("1 Register new property");
            System.out.println("2 Print out all registered properties");
            System.out.println("3 Search for a particular property");
            System.out.println("4 Calculate the average area of each property");
            System.out.println("5 Remove a porperty the register");
            System.out.println("0 end the program ");

            int valg = scanner.nextInt();
            scanner.nextLine();

            
         
            switch(valg){
                case 1: 
                    System.out.println("");
                    System.out.println("You have chosen 1: to register a new property");
                    System.out.println("Insert the municipality number: ");
                    int mNumber = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Insert the municipality name: ");
                    String mName = scanner.nextLine();
                    System.out.println("Insert the lot number:");
                    int lotNumber = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Insert the lot name:");
                    String nameLot = scanner.nextLine();
                    System.out.println("Insert the section number:");
                    int sectionNumber = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Insert the area of the property:");
                    int area = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Insert the name of the owner");
                    String nameOwner = scanner.nextLine();

                    Property newProperty = new Property(mNumber, mName, lotNumber, sectionNumber, nameLot, area, nameOwner);
                    propertyReg.addProperty(newProperty);
                    System.out.println("The property has successfully been registered");
                    break;
                case 2:
                    System.out.println("");
                    System.out.println("You have chosen 2: to print all registered properties");
                    System.out.println(propertyReg.toString());
                    break;
                case 3: 
                    System.out.println("");
                    System.out.println("You have chosen 3: to find a particular property");
                    System.out.println("Insert the municipality number");
                    int mNum = scanner.nextInt();
                    System.out.println("Insert the section number");
                    int sNum = scanner.nextInt();
                    System.out.println("Insert the lot number");
                    int lNum = scanner.nextInt();
                    if (propertyReg.checkRegisterContains(mNum, sNum, lNum)){
                        System.out.println("");
                        System.out.println("This property matches your requirements");
                        System.out.println(propertyReg.findProperties(mNum, sNum, lNum));
                    } else {
                        System.out.println("That property is not registered");
                    }
                    break;

                case 4: 
                    System.out.println("");
                    System.out.println("You have chosen 4: to find average area of all the registered properties");
                    System.out.println("Average area of the properties is: " + propertyReg.findAverageArea());
                    break;
                case 5: 
                    System.out.println("");
                    System.out.println("You have chosen 5: to remove a properti from the register");
                    System.out.println("Insert the municipality number");
                    int mNumb = scanner.nextInt();
                    System.out.println("Insert the section number");
                    int sNumb = scanner.nextInt();
                    System.out.println("Insert the lot number");
                    int lNumb = scanner.nextInt();
                    propertyReg.removeProperty(mNumb, sNumb, lNumb);
                    break;
                case 0:
                    System.out.println("The program ends");
                    scanner.close();
                    System.exit(0);
                default: 
                    System.out.println("Invalid input, try again! "); 
            }
        }




    }//end main

/* 
    public void testData(){
        
        propertyRegister propertyReg = new propertyRegister();

        property property1 = new property(1445,"Gloppen",77,631,1017,"JensOlsen");
        property property2 = new property(1445,"Gloppen",77,131,"Syningom", 650,"Nicolay Madsen");
        property property3 = new property(1445, "Gloppen",75,19,"Fugletun",650,"Evilyn Jenson");
        property property4 = new property(1445,"Gloppen", 74,188,1457, "Karl Ove Braaten");
        property property5 = new property(1445,"Gloppen",69,47,1339,"Elsa IndreGaard");

        propertyReg.addProperty(property1);
        propertyReg.addProperty(property2);
        propertyReg.addProperty(property3);
        propertyReg.addProperty(property4);
        propertyReg.addProperty(property5);
        }
 */

} //end client class


       
    
 
 




