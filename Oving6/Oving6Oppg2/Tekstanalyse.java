// Tekstanalyse.java
public class Tekstanalyse {
    private int[] antallTegn = new int[30];


    public Tekstanalyse(String tekst) {
        // konstruktør
        for (int i = 0; i < tekst.length(); i++) {
            char tegn = tekst.charAt(i);
            if (tegn >= 'a' && tegn <= 'å') {
                antallTegn[tegn - 'a']++;
            } else if (tegn >= 'A' && tegn <= 'Å') {
                antallTegn[tegn - 'A']++;
            } else {
                antallTegn[29]++; 
            }
        }
    }

    public int finnAntallForskjelligeBokstaver() {
        int antall = 0;
        for (int i = 0; i < 29; i++) {
            if (antallTegn[i] > 0) {
                antall++;
            }
        }
        return antall;
    }

    public int finnTotaltAntallBokstaver() {
        int totaltAntall = 0;
        for (int i = 0; i < 29; i++) {
            totaltAntall += antallTegn[i];
        }
        return totaltAntall;
    }

    public double prosentIkkeBokstaver() {
        double prosent = (double) antallTegn[29] / finnTotaltAntallBokstaver() * 100;
        return prosent;
    }

    public int finnAntallForekomsterAvBokstav(char bokstav) {
        if (bokstav >= 'a' && bokstav <= 'å') {
            return antallTegn[bokstav - 'a'];
        } else if (bokstav >= 'A' && bokstav <= 'Å') {
            return antallTegn[bokstav - 'A'];
        } else {
            return 0;
        }
    }

    public char[] finnBokstaverMedMaksForekomster() {
        int maksAntall = 0;
        for (int i = 0; i < 29; i++) {
            if (antallTegn[i] > maksAntall) {
                maksAntall = antallTegn[i];
            }
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 29; i++) {
            if (antallTegn[i] == maksAntall) {
                char bokstav = (char) (i + 'a');
                result.append(bokstav);
            }
        }
        return result.toString().toCharArray();
    }
}
