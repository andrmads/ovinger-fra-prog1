// Klient.java
import java.util.Scanner;

public class Klient {
    public static void kjorKlient() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Skriv inn tekst (eller avslutt med 'q'): ");
            String tekst = scanner.nextLine();

            if (tekst.equals("q")) {
                break;
            }

            Tekstanalyse analyse = new Tekstanalyse(tekst);

            System.out.println("Antall forskjellige bokstaver: " + analyse.finnAntallForskjelligeBokstaver());
            System.out.println("Totalt antall bokstaver: " + analyse.finnTotaltAntallBokstaver());
            System.out.println("Prosent ikke-bokstaver: " + analyse.prosentIkkeBokstaver() + "%");

            System.out.print("Skriv inn en bokstav for å finne antall forekomster: ");
            char bokstav = scanner.nextLine().charAt(0);
            System.out.println("Antall forekomster av '" + bokstav + "': " + analyse.finnAntallForekomsterAvBokstav(bokstav));

            char[] mestFrekventeBokstaver = analyse.finnBokstaverMedMaksForekomster();
            System.out.print("Mest frekvente bokstav(er): ");
            for (char b : mestFrekventeBokstaver) {
                System.out.print(b + " ");
            }
            System.out.println();
        }
        scanner.close();
    }
}

