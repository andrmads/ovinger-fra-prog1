import java.util.Random;

// bruker første plassering i listen til å bety tall nummer 1
// så fyller jeg plasseringen med det antallet jeg har fått av "1"
//dersom jeg har fått tre 1ere vil første tall i listen være 3


public class oppg1{
    public static void main(String[] args) {
        Random random = new Random();
        int[] antallForekomster = new int[10]; // En array for å lagre antall forekomster


        for (int i = 0; i < 1000; i++) { // Gjenta løkken 1000 ganger som beskrevet
            int tall = random.nextInt(10); // Generer tilfeldig tall mellom 0 og 9
            //System.out.println("tilfeldig tall: " + tall);                                                  # for å forstå
            antallForekomster[tall]++; // Øk telleren for riktig tall
            //System.out.println("antallForekomster[tall] betyr her: " + antallForekomster[tall]);             # for å forstå
        }

        System.out.println("");
        // Skriv ut antall forekomster av hvert tall
        for (int i = 0; i < antallForekomster.length; i++) {
            System.out.println("Antall forekomster av " + i + ": " + antallForekomster[i]);
        }
       
        System.out.println(antallForekomster[0]);
    }
}
