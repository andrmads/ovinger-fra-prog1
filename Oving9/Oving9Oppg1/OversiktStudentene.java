
import java.util.ArrayList;

public class OversiktStudentene {
    private ArrayList<Student> studenter;
    private int numberOfStudents = 0;

    public OversiktStudentene(){
        studenter = new ArrayList<>();
    }

    public int getAntStud(){
        int antStud = studenter.size();
        return antStud;
    }
    public ArrayList<Student> getStudenter(){
        return studenter;
    }
    
    public void registrerNyStudent(String navn, int antOppg){
        studenter.add(new Student(navn, antOppg));
        numberOfStudents += 1;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("oversikt over Studenter: \n");

        for (Student student : studenter){
            sb.append(student).append("\n");
            //sb.append("Navn: ").append(student.getNavn()).append(", Antall oppgaver: ").append(student.getAntOppg()).append("\n");
        }
        return sb.toString();
    }




    }







