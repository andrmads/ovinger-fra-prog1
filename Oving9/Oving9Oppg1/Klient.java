import java.util.ArrayList;
import java.util.Scanner;
public class Klient {
    public static void main (String [] args){
        
        Scanner scanner = new Scanner(System.in);
        boolean KjørProgram = true;
        OversiktStudentene oversikt = new OversiktStudentene();
        ArrayList<Student> studentListe = oversikt.getStudenter();

        oversikt.registrerNyStudent("Ola", 1); 
        oversikt.registrerNyStudent("Per", 3); 
        oversikt.registrerNyStudent("Jon", 5);

        while (KjørProgram){
            System.out.println("");
            System.out.println("Du får nå 5 valg: ");
            System.out.println("1. se antall registrerte studenter");
            System.out.println("2. se alle studenter med antall godkjente oppgaver");
            System.out.println("3. se en spesifikk student med antall godkjente oppgaver");
            System.out.println("4. registrere ny student");
            System.out.println("5. endre antall godkjente oppgaver for en student");
            System.out.println("Trykk på 1, 2, 3, 4 eller 5 ");
            System.out.println("");

            int valg = scanner.nextInt();
            scanner.nextLine();

            switch(valg){
                case 1: //se antall registrerte studenter
                    System.out.println("Antall studenter: " + oversikt.getAntStud());
                    break;

                case 2:  // se alle studenter med antall godkjente oppgaver
                    System.out.println("Oversikt over alle studenter med antall godkjente oppgaver: ");
                    System.out.println(oversikt.toString());
                    break;

                case 3:  // se en spesifikk student med antall godkjente oppgaver
                    System.out.println("Skriv inn navnet på studenten du vil se");
                    String navn = scanner.nextLine();
                    
                    for (int i = 0; i < studentListe.size(); i++){
                        if (navn.equalsIgnoreCase(studentListe.get(i).getNavn())){
                            System.out.println("Navn på studenten: " + studentListe.get(i).getNavn());
                            System.out.println("Antall godkjente oppgaver: " + studentListe.get(i).getAntOppg()); 
                        }
                        
                    }
                    break;

                case 4:   // registrere ny student 
                    System.out.println("Hva heter studenten?");
                    String Navn = scanner.nextLine();
                    System.out.println("Hvor mange oppgaver har han utført?");
                    int oppg = scanner.nextInt();
                    
                    oversikt.registrerNyStudent(Navn, oppg);
                    System.out.println("Vi har lagt til den nye studenten");
                    break; 

                case 5:    // endre antall godkjente oppgaver for en student
                    System.out.println("Skriv inn navnet på studenten du vil se");
                    navn = scanner.nextLine();
                    
                    for (int i = 0; i < studentListe.size(); i++){
                        if (navn.equalsIgnoreCase(studentListe.get(i).getNavn())){
                            System.out.println(studentListe.get(i).getNavn() + " har " + studentListe.get(i).getAntOppg() + " poeng før økning ");
                            studentListe.get(i).økAntOppg(1); 
                            System.out.println("Nå har " + studentListe.get(i).getNavn() +" " + studentListe.get(i).getAntOppg() + " poeng");
                        }
                        
                    }
                    break;







            }

        }









        
        
        



        scanner.close();
    }
}
