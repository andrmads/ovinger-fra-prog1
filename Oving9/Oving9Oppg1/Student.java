

public class Student {
    private String navn;
    private int antOppg;

    public Student(String navn, int antOppg){
        this.navn = navn;
        this.antOppg = antOppg;
    }

    public String getNavn(){
        return navn;
    }
    public int getAntOppg(){
        return antOppg;
    }
    public int økAntOppg(int økning){   
        antOppg = antOppg + økning;
        return antOppg;
    }
    public String toString(){
        return "Student:Navn=" + navn + ", antOppg= " + antOppg ;
    }




}

