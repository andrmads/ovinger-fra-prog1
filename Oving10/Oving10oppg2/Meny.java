
import java.util.List;
import java.util.ArrayList;
public class Meny{

    private List<Rett> retter = new ArrayList<>();

    public void leggTilRett(Rett rett){
        retter.add(rett);
    }

    public double getTotalPris(){
        double totalPris = 0;
        for (Rett rett : retter){
            totalPris+=rett.getPris();
        }
        return totalPris;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Meny: \n");
        for (Rett rett : retter){
            sb.append(" - ").append(rett.getNavn()).append(": ").append(rett.getPris()).append(" NOK\n");
        }
        sb.append("Totalpris. ").append(getTotalPris()).append(" NOK");
        return sb.toString();
    }






}