import java.util.ArrayList;
import java.util.List;


public class MenyRegister {
    private List<Rett> retter = new ArrayList<>();
    private List<Meny> menyer = new ArrayList<>();

    public void registrerRett(Rett rett){
        retter.add(rett);
    }

    public Rett finnRett(String navn){
        for (Rett rett : retter){
            if (rett.getNavn().equals(navn)){
                return rett;
            }
        }
        return null; // returnerer null hvis ikke retten finnes
    }
    public List<Rett> finnRetterEtterType(String type){
        List<Rett>resultater = new ArrayList();
        for (Rett rett : retter){
            if (rett.getType().equalsIgnoreCase(type)){
                resultater.add(rett);
            }
        }
        return resultater;
    }

    public void registrerMeny(Meny meny){
        menyer.add(meny);
    }

    public List<Meny> finnMenyerMedTotalPris(double minPris, double maksPris){
        List<Meny> resultater = new ArrayList();
        for (Meny meny : menyer){
            double totalPris = meny.getTotalPris();
            if (totalPris >= minPris && totalPris <= maksPris){
                resultater.add(meny);
            }
        } 
        return resultater;
    }





}
