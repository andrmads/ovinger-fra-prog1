import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenyKlient {
    public static void main(String []args){

        MenyRegister menyRegister = new MenyRegister();
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("");
            System.out.println("Velg en operasjon:");
            System.out.println("1. Registrer en ny rett");
            System.out.println("2. Finn en rett etter navn");
            System.out.println("3. Finn retter etter type");
            System.out.println("4. Registrer en ny meny");
            System.out.println("5. Finn menyer med totalpris innenfor et prisintervall");
            System.out.println("0. Avslutt programmet");

            int valg = scanner.nextInt();
            scanner.nextLine();

            switch(valg){
                case 1: 
                    System.out.println("Skriv inn navn på retten:");
                    String navn = scanner.nextLine();
                    System.out.println("Skriv inn type (forrett, hovedrett, dessert osv):");
                    String type = scanner.nextLine();
                    System.out.println("Skriv inn pris:");
                    double pris = scanner.nextDouble();
                    scanner.nextLine(); // Leser av newline
                    System.out.println("Skriv inn oppskrift:");
                    String oppskrift = scanner.nextLine();
                    Rett rett = new Rett(navn, type, pris, oppskrift);
                    menyRegister.registrerRett(rett);
                    break;
                case 2:
                    System.out.println("Skriv inn navnet på retten du vil finne:");
                    String rettNavn = scanner.nextLine();
                    Rett funnetRett = menyRegister.finnRett(rettNavn);
                    if (funnetRett != null) {
                        System.out.println("Rett funnet: " + funnetRett.getNavn() + " - Pris: " + funnetRett.getPris() + " NOK");
                    } else {
                        System.out.println("Rett ikke funnet.");
                    }
                    break;
                case 3: 
                    System.out.println("Skriv inn typen (forrett, hovedrett, dessert osv) du vil søke etter:");
                    String rettType = scanner.nextLine();
                    List<Rett> retterEtterType = menyRegister.finnRetterEtterType(rettType);
                    if (!retterEtterType.isEmpty()) {
                        System.out.println("Retter funnet etter type " + rettType + ":");
                        for (Rett retten : retterEtterType) {
                            System.out.println(retten.getNavn() + " - Pris: " + retten.getPris() + " NOK");
                        }
                    } else {
                        System.out.println("Ingen retter funnet etter type " + rettType + ".");
                    }
                    break;
                case 4:
                    Meny meny = new Meny();
                    System.out.println("Legg til retter i menyen (skriv 'avslutt' for å avslutte):");
                    while (true) {
                        System.out.println("Skriv inn navn på retten du vil legge til:");
                        String menyRettNavn = scanner.nextLine();
                        if (menyRettNavn.equalsIgnoreCase("avslutt")) {
                            break;
                        }
                        Rett menyRett = menyRegister.finnRett(menyRettNavn);
                        if (menyRett != null) {
                            meny.leggTilRett(menyRett);
                            System.out.println("Rett lagt til i menyen.");
                        } else {
                            System.out.println("Rett ikke funnet. Vennligst legg til retten først.");
                        }
                    }
                    menyRegister.registrerMeny(meny);
                    break;
                case 5: 
                    System.out.println("Skriv inn minimumspris:");
                    double minPris = scanner.nextDouble();
                    System.out.println("Skriv inn maksimumspris:");
                    double maksPris = scanner.nextDouble();
                    List<Meny> funnetMenyer = menyRegister.finnMenyerMedTotalPris(minPris, maksPris);
                    if (!funnetMenyer.isEmpty()) {
                        System.out.println("Menyer funnet innenfor prisintervall " + minPris + " NOK - " + maksPris + " NOK:");
                        for (Meny menyen : funnetMenyer) {
                            System.out.println(menyen.toString());
                        }
                    } else {
                        System.out.println("Ingen menyer funnet innenfor prisintervall " + minPris + " NOK - " + maksPris + " NOK.");
                    }
                    break;
                case 0:
                    System.out.println("Programmet avsluttes.");
                    scanner.close();
                    System.exit(0);
                default: 
                    System.out.println("Ugyldig valg. Prøv igjen.");

            } /// swtich



        } ///while true















    } //main
} //class
