

public class Arrangement{

    private int arrangementNummer;
    private String navn; 
    private String sted;
    private String arrangør;
    private String type;
    private long tidspunkt; 

    public Arrangement(int arrangementNummer, String navn, String sted, String arrangør, String type, long tidspunkt){
        this.arrangementNummer = arrangementNummer; 
        this.navn = navn;
        this.sted = sted;
        this. arrangør = arrangør; 
        this.type = type;
        this.tidspunkt = tidspunkt; 

    }
    public int getArrangementNummer(){
        return arrangementNummer; 
    }
    public String getNavn(){
        return navn;
    }
    public String getSted(){
        return sted;
    }
    public String getArrangør(){
        return arrangør;
    }
    public String getType(){
        return type;
    }
    public long getTidspunkt(){
        return tidspunkt;
    }





}