import java.util.Scanner;
import java.util.List;

public class Klient {
    public static void main(String []args){

        ArrangementRegister arrangementRegister = new ArrangementRegister();
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("");
            System.out.println("Velg en operasjon: ");
            System.out.println("1 registerer et nytt arrangement");
            System.out.println("2 Finn arrangementer på et gitt sted");
            System.out.println("3 Finn arrangementer på en gitt dato");
            System.out.println("4 Finn arrangementer innenfor et tidsintervall");
            System.out.println("5 Lag lister over arrangementer etter sted, type og tidspunkt");
            System.out.println("0 avslutt programmet");

            int valg = scanner.nextInt();
            scanner.nextLine();

            switch(valg){
                case 1: 
                    System.out.println("Skriv inn arrangements nummer: ");
                    int nummer = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("skriv inn arrangementets navn: ");
                    String navn = scanner.nextLine();
                    System.out.println("Skriv inn sted:");
                    String sted = scanner.nextLine();
                    System.out.println("Skriv inn arrangørens navn:");
                    String arrangør = scanner.nextLine();
                    System.out.println("Skriv inn type (konsert, barneteater, foredrag osv):");
                    String type = scanner.nextLine();
                    System.out.println("Skriv inn dato (eks 30 okt 2002 blir 20021030):");
                    System.out.println("år, mnd, dato");
                    long tidspunkt = scanner.nextLong();
                    Arrangement nyttArrangement = new Arrangement(nummer, navn, sted, arrangør, type, tidspunkt);
                    arrangementRegister.registrerArrangement(nyttArrangement);
                    break;
                case 2: 
                    System.out.println("Skriv inn sted: ");
                    String stedToFind = scanner.nextLine();
                    List<Arrangement>arrangementerPåSted = arrangementRegister.finnArrangementerPåSted(stedToFind);
                    //skriv ut arrangementeene på stedet
                    for (Arrangement arrangement : arrangementerPåSted){
                        System.out.println(arrangement.getNavn());
                    }
                    break; 
                case 3: 
                    System.out.println("Skriv inn dato (som heltall, for eksempel 20021030 for 30.oktober 2002):");
                    long datoToFind = scanner.nextLong();
                    List<Arrangement> arrangementerPåDato = arrangementRegister.finnArrangementerPåDato(datoToFind);
                    // Skriv ut arrangementene på datoen
                    for (Arrangement arrangement : arrangementerPåDato) {
                        System.out.println(arrangement.getNavn());
                    }
                    break;
                case 4:
                    // Les inn start- og sluttdato fra brukeren og finn arrangementer innenfor dette tidsintervall
                    System.out.println("Skriv inn startdato (som heltall):");
                    long startDato = scanner.nextLong();
                    System.out.println("Skriv inn sluttdato (som heltall):");
                    long sluttDato = scanner.nextLong();
                    List<Arrangement> arrangementerInnenTidsintervall = arrangementRegister.finnArrangementerInnenTidsintervall(startDato, sluttDato);
                    // Skriv ut arrangementene innenfor tidsintervallet
                    for (Arrangement arrangement : arrangementerInnenTidsintervall) {
                        System.out.println(arrangement.getNavn());
                    }
                    break;
                case 5:
                    // Implementer metoder for å lage lister etter sted, type og tidspunkt
                    List<Arrangement> arrangementerSortertEtterStedTypeTidspunkt = arrangementRegister.lagListeSortertEtterStedTypeTidspunkt();
                    // Skriv ut arrangementene etter sted, type og tidspunkt
                    for (Arrangement arrangement : arrangementerSortertEtterStedTypeTidspunkt) {
                        System.out.println("Sted: " + arrangement.getSted() + ", Type: " + arrangement.getType() + ", Navn: " + arrangement.getNavn());
                    }
                    break;
                case 0:
                    System.out.println("Programmet avsluttes.");
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Ugyldig valg. Prøv igjen.");
                
            }

        }
        




    }
}
