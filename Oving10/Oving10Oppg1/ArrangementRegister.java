import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ArrangementRegister {
    private List<Arrangement> arrangementer;

public ArrangementRegister(){
    arrangementer = new ArrayList<>();
    }

public void registrerArrangement(Arrangement arrangement){
    arrangementer.add(arrangement);
}

public List<Arrangement> finnArrangementerPåSted(String sted){
    List<Arrangement> resultater = new ArrayList<>();
    for (Arrangement arrangement : arrangementer){
        if (arrangement.getSted().equalsIgnoreCase(sted)){
            resultater.add(arrangement);
        }
    }
    return resultater; 
}

public List<Arrangement> finnArrangementerPåDato(long dato){
    List<Arrangement>resultater = new ArrayList<>();
    for (Arrangement arrangement : arrangementer){
        if (arrangement.getTidspunkt() == dato){  
            resultater.add(arrangement);
        }
        
    }
    return resultater;
}
public List<Arrangement> finnArrangementerInnenTidsintervall(long startDato, long sluttDato){
    List<Arrangement> resultater = new ArrayList<>();
    for (Arrangement arrangement : arrangementer){
        if(arrangement.getTidspunkt()>= startDato && arrangement.getTidspunkt() <= sluttDato){
            resultater.add(arrangement);
        }
    }
    resultater.sort((a1,a2) ->Long.compare(a1.getTidspunkt(), a2.getTidspunkt()));  
    return resultater;
}


public List<Arrangement> lagListeSortertEtterStedTypeTidspunkt() {
    List<Arrangement> sortertListe = new ArrayList<>(arrangementer);
    
    sortertListe.sort(Comparator.comparing(Arrangement::getSted)
            .thenComparing(Arrangement::getType)
            .thenComparing(Arrangement::getTidspunkt));
    
    return sortertListe;




}
}
