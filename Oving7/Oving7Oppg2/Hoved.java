public class Hoved {
    public static void main(String [] args){

        TekstBehandling tekst1 = new TekstBehandling("det er teksten", "det", " bæ ");

        System.out.println("");
        System.out.println("Forsøker å printe teksten: " + tekst1.getTekst());
        System.out.println("Forsøker å printe teksten m store bokstaver: " + tekst1.getTekstStorBokstav());
        System.out.println("Forsøker å få antall ord : " + tekst1.getAntallOrd());
        System.out.println("Forsøker å få antall bokstaver : " + tekst1.getAntallBokstaver());
        System.out.println("Forsøker å få gjS antall bokstaver pr ord : " + tekst1.getGjLengdeOrd());
        System.out.println("Forsøker å få gjS antall ord pr periode : " + tekst1.getGjLengdeOrdPrPeriode());
        System.out.println("Forsøker å endre ordet -" + tekst1.getOrdFjern() + "- til -" + tekst1.getOrdByttTil() +"- : " + tekst1.getByttOrd());
        System.out.println("");

    }
}
