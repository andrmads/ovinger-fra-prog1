
public class TekstBehandling {      //attributter
    private String Tekst;
    private String OrdFjern;
    private String OrdByttTil;   

public TekstBehandling(String Tekst, String OrdFjern, String OrdByttTil){   //konstruktør
    this.Tekst = Tekst;
    this.OrdFjern = OrdFjern;
    this.OrdByttTil = OrdByttTil;
}


public String getTekst(){               //metoden gir teksten
    return Tekst;
}
public String getOrdFjern(){               //metoden gir Ord Som skal fjernes
    return OrdFjern;
}
public String getOrdByttTil(){               //metoden gir ord som skal byttes til
    return OrdByttTil;
}
public String getTekstStorBokstav(){    //metode gir tekst i store bokstaver
    String StorBokTekst = Tekst.toUpperCase();
    return StorBokTekst;
}

public int getAntallOrd(){              // metoden gir antall ord i teksten
    String [] ordListe = Tekst.split(" ");
    int antallOrd = ordListe.length;
    return antallOrd; 
}
public String getGjLengdeOrd(){         //metoden gir gjennomsnitts lengde pr ord
    String [] ordListe = Tekst.split(" ");
    int antallOrd = ordListe.length;
    double totAntBokstaver = 0;

    for (String ord : ordListe){                //iterer gjennom listen ordListe for hvert ord
        int antBokstaver = ord.length();
        totAntBokstaver +=antBokstaver;
    }
    double gjSnitt = totAntBokstaver/antallOrd;
    String formatertGjSnitt = String.format("%.2f", gjSnitt);   //setter antall desimaler til 2 ved stringformatering
    return formatertGjSnitt;
}

public String getGjLengdeOrdPrPeriode(){        // metoden gir gjSnitt lengde på ord pr periode
    String [] perioder = Tekst.split("[.!?\\:]");
    int antallPerioder = perioder.length;
    double totAntOrd = 0;

    for (String Periode : perioder){
        String [] ord = Periode.trim().split("\\s+");
        totAntOrd += ord.length;
    }
    double GjOrdPrPeriode = totAntOrd/antallPerioder;
    String formatGjOrdPrPeriode = String.format("%.2f", GjOrdPrPeriode);
    return formatGjOrdPrPeriode;
}


public int getAntallBokstaver(){         //metoden gir antall bokstaver
    String [] ordListe = Tekst.split(" ");
    int totAntBokstaver = 0;
    for (String ord : ordListe){                //iterer gjennom listen ordListe for hvert ord
       int antBokstaver = ord.length();
       totAntBokstaver +=antBokstaver;
    }
    return totAntBokstaver;
}

public String getByttOrd(){             // metoden bytter et ord med et annet
    Tekst = Tekst.toLowerCase();       //gjør teksten til små bokstaver
    OrdFjern.toLowerCase();             //gjør ordFjern til små bokstaver
    boolean inneholderTekst = Tekst.contains(OrdFjern);     //boolean er True dersom Tekst inneholder OrdFjern
    if (inneholderTekst){                                   //dersom True byttes OrdFjern med OrdTtil
        Tekst = Tekst.replace(OrdFjern, OrdByttTil);
    } else{
    }
    return Tekst;
}




}
