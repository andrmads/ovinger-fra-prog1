public class Hoved{
    public static void main(String[] args){

        NyString tekst1 = new NyString("Dette er en test", 'e');


        System.out.println("Dette er teksten:  " + tekst1.getTekst());
        System.out.println("Tester om jeg får forkortet tekst:  " + tekst1.getForkortning());
        System.out.println("Tester om jeg får tekst uten bokstaven -" + tekst1.getBokstav() + "- : " + tekst1.getTekstMinusBokstav());

    }
}
