public class NyString {
    private String Tekst;
    private char Bokstav;

public NyString(String Tekst, char Bokstav){
    this.Tekst = Tekst;
    this.Bokstav = Bokstav;
}   
public String getTekst(){
    return Tekst;
}
public char getBokstav(){
    return Bokstav;
}
public String getForkortning(){
    String kopiTekst = Tekst;                           // siden skulle være immutable tar jeg en kopi som jeg endrer på
    String forkortningen = "";                          //variabel som skal inneholde første bokstav i hvert ord, initierer som tom
    String[] ordListe = kopiTekst.split(" ");   // har liste hvor hvert ord er et element i listen
    
    for (int i=0;i<ordListe.length;i++){
        String ord = ordListe[i];                       // iterer gjennom isten med ord 

        if (!ord.isEmpty()){                                
            char fBokstav = ord.charAt(0);         // finner første bokstav i ordet
            forkortningen = forkortningen + fBokstav;   // legger denne bokstaven til variabelen "forkortningen"
        }
    }
    return forkortningen;
}
public String getTekstMinusBokstav(){
    String kopiTekst = Tekst;
    String[] ordListe = kopiTekst.split("");
    String TekstMinusBokstav = "";
     
    for (String ord : ordListe){            //iterer gjennom hvert element (ord) i listen ordListe
        String nyttOrd = "";
        for (int j=0;j<ord.length();j++){
            char BokstavFraTekst = ord.charAt(j);
            if(BokstavFraTekst != Bokstav){
                nyttOrd += BokstavFraTekst;
            }
        }
        TekstMinusBokstav += nyttOrd;
    }
    return TekstMinusBokstav;

}



}
