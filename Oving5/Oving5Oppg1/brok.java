public class brok {
    private int teller;
    private int nevner;

    public brok(int teller, int nevner){
        if (nevner==0){
            throw new IllegalArgumentException("Nevner kan ikke være 0");
        } 
        this.teller = teller;
        this.nevner = nevner; 
    }
    public brok(int teller){
        this.teller = teller;
        this.nevner = 1;
    }
    public int getTeller(){
        return teller;
    }
    public int getNevner(){
        return nevner;
    }


    public brok getResAdd(brok annenBrok){
        int nyTeller = this.teller*annenBrok.nevner + annenBrok.teller*this.nevner;
        int nyNevner = this.nevner*annenBrok.nevner;
        return new brok(nyTeller, nyNevner);
    }

    public brok getResSub(brok annen){
        int nyTeller = this.teller*annen.nevner - annen.teller*this.nevner;
        int nyNevner = this.nevner*annen.nevner;
        return new brok(nyTeller, nyNevner);
    }

    public brok getResMulti(brok annenBrok){
        int nyTeller = this.teller*annenBrok.teller;
        int nyNevner = this.nevner*annenBrok.nevner;
        return new brok(nyTeller, nyNevner);
    }

    public brok getResDivi(brok annen){
        if (annen.teller==0){
            throw new IllegalArgumentException("Du kan ikke dele med 0");
        }
        int nyTeller = this.teller*annen.nevner;
        int nyNevner = this.nevner*annen.teller;
        return new brok(nyTeller, nyNevner);
    }
    public String toString() {
        return teller + "/" + nevner;
    }




}
