import java.util.Random;
public class MinRandom {
    
    private Random randomGenerator;

    public MinRandom() {
        randomGenerator = new Random();
    }

    public int nesteHeltall(int nedre, int ovre) {
        // Genererer et tilfeldig heltall i intervallet [nedre, ovre]
        return randomGenerator.nextInt(ovre - nedre + 1) + nedre;
    }

    public double nesteDesimaltall(double nedre, double ovre) {
        // Genererer et tilfeldig desimaltall i intervallet [nedre, ovre)
        return randomGenerator.nextDouble() * (ovre - nedre) + nedre;
    }
}

  






    
