public class mainFil {
    

    public static void main(String[] args) {
        MinRandom minRandom = new MinRandom();

        // Eksempel på bruk av nesteHeltall-metoden
        int tilfeldigHeltall = minRandom.nesteHeltall(5, 10);
        System.out.println("Tilfeldig heltall mellom 5 og 10: " + tilfeldigHeltall);

        // Eksempel på bruk av nesteDesimaltall-metoden
        double tilfeldigDesimaltall = minRandom.nesteDesimaltall(2.0, 10.0);
        System.out.println("Tilfeldig desimaltall mellom 2.0 og 10.0: " + tilfeldigDesimaltall);
    }
}


