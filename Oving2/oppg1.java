/*
 Oppgave 1
Et år er skuddår dersom det er delelig med 4. Unntaket er hundreårene, de må være delelig 
med 400.
Tegn aktivitetsdiagram som viser algoritmen for å finne ut om et år er skuddår. Årstallet skal 
leses inn fra brukeren. Sett opp testdata. Lag og prøv ut programmet 
 */


import java.util.Scanner;
public class oppg1 {
    public static void main(String[] args){
         
        Scanner scanner = new Scanner(System.in);

        System.out.println("Hvilket årstall er det nå?");
        int aarstall = scanner.nextInt();


        if (aarstall%4==0 && aarstall%100!=0 ||aarstall%400==0) {
            System.out.println("Det er skuddår!");
        } else {
            System.out.println("Det er ikke skuddår...");
        }

        scanner.close();























/* 

        Scanner scannerTwo = new Scanner(System.in);

        System.out.println("Hva heter du?"); 
        String brukerNavn = scannerTwo.nextLine();
        System.out.printf("Hei %s", brukerNavn);

        scannerTwo.close();

*/
        
    }
}