/*
  Lag et program som hjelper oss i forhold til følgende problemstilling: Kjøttdeig av merke A 
koster kr 35,90 for 450 gram, mens kjøttdeig av merke B koster kr 39,50 for 500 gram. 
Hvilket merke er billigst?
 */


public class oppg2 {
    public static void main(String[] args) {

        double prisA = 35.90;
        double vektA = 450;
        double prisB = 39.50;
        double vektB = 500; 

        double prisPrGramAAA = prisA/vektA;
        double prisPrGramBBB = prisB/vektB; 

        if (prisPrGramAAA<prisPrGramBBB){
            System.out.print("Produkt A er billigst");
        } else if (prisPrGramAAA>prisPrGramBBB){
            System.out.println("Produkt B er billgist");
        } else {
            System.out.println("De er like billige");

        }

    }
    
}
